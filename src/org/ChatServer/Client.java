package org.ChatServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;

public class Client extends Thread {

  private final Socket clientSocket;
  private String name;
  private final Server server;
  private OutputStream outputStream;
  private InputStream inputStream;

  public Client(Server server2, Socket clientSocket, String clientName) {
    this.clientSocket = clientSocket;
    this.server = server2;
    this.name = clientName;
  }

  @Override
  public void run() {
    try {
      //
      handleClientSocket();
      if (!(clientSocket.isClosed()))
        handleLogoff();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void handleClientSocket() throws IOException, InterruptedException {
    this.inputStream = clientSocket.getInputStream();
    this.outputStream = clientSocket.getOutputStream();

    BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
    String s;
    while ((s = br.readLine()) != null) {

      if (s != null && s.length() > 0) {
        if ("quit".equalsIgnoreCase(s)) {
          handleLogoff();
          break;
        }

        else
          handleChat(s);
      }
    }



  }


  private void handleLogoff() throws IOException {

    String s = name + " just logged off";
    List<Client> workerList = server.getWorkerList();
    for (Client worker : workerList) {
      if (!(this.name == worker.name))
        worker.send(s);
    }

    server.removeWorker(this);
    clientSocket.close();
  }

  private void handleChat(String chat) throws IOException {
    String msg = (name + ": " + chat + " \r\n");

    List<Client> workerList = server.getWorkerList();
    for (Client worker : workerList) {
      worker.send(msg);
    }
  }

  private void send(String msg) throws IOException {

    msg = msg + "\r\n";
    this.outputStream.write(msg.getBytes());

  }


}
