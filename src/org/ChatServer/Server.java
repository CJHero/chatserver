package org.ChatServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server extends Thread {

  private int serverPort;
  private String clientName;
  private ArrayList<Client> clients = new ArrayList<Client>();
  private int i = 0;

  public Server(int serverPort) {
    this.serverPort = serverPort;
  }

  @Override
  public void run() {
    try {
      ServerSocket serverSocket = new ServerSocket(serverPort);
      while (true) {
        System.out.println("Waiting to connect......");
        final Socket clientSocket = serverSocket.accept();
        System.out.println("You are connected, to exit the chat just type quit. Be nice! :) \r\n ");
        clientName = "Client#" + i++;
        Client s1 = new Client(this, clientSocket, clientName);
        clients.add(s1);
        s1.start();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public List<Client> getWorkerList() {
    return clients;
  }

  public void removeWorker(Client s) {
    clients.remove(s);
  }
}
